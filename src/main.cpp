/*
 * Sample program for DokodeModem
 * SD read/write sample
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
//ライブラリはこちらを利用させても雷っていますが、一部エラーがでるので修正しています。https://github.com/adafruit/SD
#include "SD.h"

DOKODEMO Dm = DOKODEMO();

bool sdcardtest()
{
  digitalWrite(SD_PW, HIGH);//Power ON
  pinMode(SD_CS, OUTPUT);
  if (!SD.begin(SD_CS))
  {
    SerialDebug.println("SD card begin error");
    return false;
  }

  SerialDebug.println("SD card open");
  File dataFile = SD.open("/datalog.txt", FILE_WRITE);
  if (dataFile)
  {
    dataFile.println("HelloWorld");
    dataFile.flush();
    dataFile.close();
  }
  else
  {
    SerialDebug.println("SD card open error");
    return false;
  }

  SerialDebug.println("SD card read check");
  dataFile = SD.open("/datalog.txt");
  if (dataFile)
  {
    String str;
    while (dataFile.available())
    {
      str += char(dataFile.read());
    }
    if (str.indexOf("HelloWorld") == -1)
    {
      SerialDebug.println("SD card read error");
      return false;
    }
  }
  else
  {
    SerialDebug.println("SD card write error");
    return false;
  }

  SerialDebug.println("SD card file erase");
  if (!SD.remove((char *)"/datalog.txt"))
  {
    SerialDebug.println("SD card erase error");
    return false;
  }

  SerialDebug.println("SDOK");

  digitalWrite(SD_PW, LOW);//Power OFF

  return true;
}

void setup()
{
  delay(3000);//USBデバッグ表示用に認識まで遅らせます。

  Dm.begin(); // 初期化が必要です。

  // デバッグ用　USB-Uart
  SerialDebug.begin(115200);

  sdcardtest();
}

void loop()
{
}
